package exemploRelacionamento;

import java.util.ArrayList;

public class Compra {

    public ArrayList<Item> items = new ArrayList<>();

    public void adicionarItems(Item item) {
        this.items.add(item);
    }

    public ArrayList<Item> getAllItems() {
        return this.items;
    }

}
