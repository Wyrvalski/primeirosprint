package exemploRelacionamento;

import java.util.ArrayList;

public class Item {

    public int quantidade;
    public Produto produto = new Produto();

    public Item(String nome, double preco, int quantidade) {
        this.produto.adicionarProduto(nome,preco);
        this.quantidade = quantidade;
    }

}
