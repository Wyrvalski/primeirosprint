package modificadoresDeAcesso.pacote1;

import modificadoresDeAcesso.pacote2.Carro;

public class Main {

    public static void main(String[] args) {
        Pessoa pessoa = new Pessoa();
        Carro carro = new Carro();
        pessoa.setNome("Eduardo");
        pessoa.setIdade(20);
        carro.setNome("Corsa");
        carro.setAno(2000);
    }
}
