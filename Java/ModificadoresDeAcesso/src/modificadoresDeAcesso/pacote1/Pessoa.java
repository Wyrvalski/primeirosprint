package modificadoresDeAcesso.pacote1;

public class Pessoa {

    private String nome;
    private Integer idade;
    private Integer altura;

    private void setNome(String nome) {
        this.nome = nome;
    }

    void setIdade(Integer idade) {
        this.idade = idade;
    }

    protected void setAltura(Integer altura) {
        this.altura = altura;
    }
}
