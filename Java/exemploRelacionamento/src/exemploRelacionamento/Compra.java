package exemploRelacionamento;

import java.util.ArrayList;

public class Compra {

    public ArrayList<Item> items = new ArrayList<>();
    public double total;

    public void adicionarItems(Item item) {
        this.items.add(item);
        setTotal(item);
    }

    public ArrayList<Item> getAllItems() {
        return this.items;
    }
    public void setTotal(Item item) {

        this.total += item.quantidade * item.produto.preco;
    }

}
