package exemploRelacionamento;

public class Cliente {

    public static void main(String[] args) {
        Compra compra = new Compra();
        compra.adicionarItems(new Item("Arroz",10.50,3));
        compra.adicionarItems(new Item("Feijão", 15.20, 2));
        for (int i = 0; i < compra.getAllItems().size(); i++) {
            System.out.println(compra.getAllItems().get(i).quantidade);
        }
        System.out.println(compra.total);
    }
}
